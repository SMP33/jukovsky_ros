#pragma once
#ifndef __NAVIGATION_NODE__
#define __NAVIGATION_NODE__
				 
#include <ros/ros.h>

#include <tuple>
#include <functional>
#include <map>

#include <GeoMath.h>
#include "jukovsky_ros/juk_dji_gps_msg.h"
#include "jukovsky_ros/juk_dji_device_status_msg.h"
#include "jukovsky_ros/juk_control_dji_msg.h"
#include "jukovsky_ros/juk_set_target_data_msg.h"
#include "jukovsky_ros/juk_position_data_msg.h"
#include "jukovsky_ros/juk_navigation_actions_msg.h"
#include <jukovsky_ros/juk_aruco_module_action.h>
#include <jukovsky_ros/juk_aruco_module_data.h>
#include <jukovsky_ros/reach_msg.h>

#include "std_msgs/String.h"

#include "ArgParser.h"


#define c(color,str)  "\x1B["<<color<<"m" << str << "\033[0m"
#define cyan_b(str)   "\x1B[46;1m" << str << "\033[0m" 
#define gray_b(str)   "\x1B[40m" << str << "\033[0m" 
#define red_b(str)   "\x1B[41;1m" << str << "\033[0m" 
#define green_b(str)   "\x1B[42;1m" << str << "\033[0m" 


#define grn(str)  "\x1B[32;1m" << str << "\x1B[39m" 
#define mgt(str)  "\x1B[35;1m" << str << "\x1B[39m" 
#define red(str)  "\x1B[31;1m" << str << "\x1B[39m" 

#define CLEAR(rows)               printf("\033[%02dA\033[0J",rows+1)




class NavigationNode
{
public:
	
	enum MODES // Перечисление позможных полетных режимов
	{
		IDLE = 0,
		// Режим ожидания, аппарат никак не управляется
		FLY_SIMPLE = 1,
		// Полет в точку по прямой с заданной скоростью
		FLY_SAFE = 2,
		// Полет в точку на безопасной высоте
		LANDING_SIMPLE = 3,
		// Сначала выполняется полет в указанную точку, затем посадка
		LANDING_ARUCO = 4,
		// Когда виден маркер, аппарат пытается на него сесть, иначе удерживает позицию.

	};
	enum SUB_MODES // Перечисление подрежимов
	{  
		BASIC_MODE = 0, 
		// Базовый подрежим
		
		FLY_SAFE_UP = 201,
		// Подрежим FLY_SAFE - аппарат сначала поднимается на безопасную высоту, затем летит к точке
		FLY_SAFE_CENTER = 202,
		// Подрежим FLY_SAFE - аппарат находится в допустимом радиусе вокруг точки и выполняет полет к ней со снижением
		
		LANDING_SIMPLE_FLY = 301,
		// Подрежим LANDING_SIMPLE - летит к указанной точке
		LANDING_SIMPLE_LAND = 302,
		// Подрежим LANDING_SIMPLE - выполняет посадку
			
		LANDING_ARUCO_FLY = 401, 
		// Подрежим LANDING_ARUCO - летит к указанной точке / удерживает точку
		LANDING_ARUCO_LAND = 402,
		// Подрежим LANDING_ARUCO - летит к маркеру
	};
	
	enum GPS_STATES
	{
		NO_SIGNAL = 0,
			// нет сигнала
		BASIC_GPS = 1,
			// используется GPS от DJI
		RTK = 2
			// используется GPS от DJI + поступают поправки RTK
	};
	
enum ACTIONS
	{
		SET_HOMEPOINT = 1  
			// устанавливает хоумпоинт
	};
	
	NavigationNode(int argc, char** argv);

private:
	
	//TODO перенести все параметры запуска на ROS
	ArgParser_Int params;    // параметры запуска
	const int max_precision_uptime = 1000000000;    // максимальное допустимое время между получениями поправок RTK
	
	void gps_callback(const jukovsky_ros::juk_dji_gps_msg::ConstPtr& input); // обрабатывает сообщение от А3 и посылает ответ
	void precision_gps_callback(const jukovsky_ros::reach_msg::ConstPtr& in); // получает поправки RTK
	void set_target_callback(const jukovsky_ros::juk_set_target_data_msg::ConstPtr& input); //обновляет текущую целевую точку
	void action_process_callback(const jukovsky_ros::juk_navigation_actions_msg::ConstPtr& input); // обрабатывает сообщение с заданной командой
	void aruco_callback(const jukovsky_ros::juk_aruco_module_data::ConstPtr& input); // получает кооридинаты аппарата относительно посадочного маркера
	
	ros::Time node_start_time;    // время запуска ноды
	
	GeoMath::v3geo	homepoint;    // начальная точка по GPS DJI
	GeoMath::v3geo	homepoint_precision;    // начальная точка с поправками RTK
	GeoMath::v3geo	current_point_abs;    // текущая точка в абс. системе с поправками RKT
	GeoMath::v3		current_velocity;    // текущая скорость
	GeoMath::v3     velocity_need; 
	GeoMath::v3     current_point_home;     // текущее смещение относительно хоумпоинта
	
	double homepoint_course;
	
	struct Target // Описывает целевую точку
	{
		GeoMath::v3geo	point_abs;     // ее абс. координаты
		uint8_t break_mode;    // режим торможения
		float cruising_speed;    // крейсерская скорость
		float accurancy;    // точность, после достижения которой точка будет считаться достигнутой
		float course;    // курс относительно севера
	}
	;
	
	struct ArUcoTarget // Описывает положение маркера
	{
		GeoMath::v3 offset;    // смещение маркера относительно текущего положения
		double course;    // курс маркера
		ros::Time uptime;    // время последнего обновления информации
		GeoMath::v3geo abs; // положение маркера в абс. координатаъ
	};
	
	struct CtrlParameters // Основные параметры системы, 
	{
		MODES MODE = MODES::IDLE; // Режим
		SUB_MODES SUB_MODE = SUB_MODES::BASIC_MODE; // Подрежим
		jukovsky_ros::juk_control_dji_msg dji_msg; // УВ
		bool stable_now; // Достиг ли аппарат цели
		
		Target target; // Текущая цель
		bool set_homepoint_flag; // Если true, то при следующем вызове gps_callback будет установлен новый хоумпоинт
	};
	
	Target pause_target;
	ArUcoTarget aruco_land;    // Вся информация о посадочном маркере
	
	GPS_STATES GPS_STATE = GPS_STATES::NO_SIGNAL;
	
	int flight_status=-1;   // Статус полета, возможные варианты в FLIGHT_STATUS_map
	
	float yaw_rate;    // требуемая угловая скорость
	
	bool stable_now;    // true: аппарат находится в допустимом радиусе возле цели в данный момент
	bool stable_last;   // stable_now за прошлую итерацию
	
	bool paused=false;
	
	double stable_time;  //время прошедшее после достижения цели
	ros::Time stable_start; // время, с которого ведется отсчет достижения цели
	ros::Time home_uptime; //время прошедшее с установки хоумпоинта
	ros::Time precision_pos_uptime; // время последнего обновления точных координат
	ros::Time last_telemetry; // время последнего вывода телеметрии для отладки
	
	ros::NodeHandle nh;					//Node Handler
	
	ros::Publisher pub_dji_control;		// Паблики
	ros::Publisher pub_position_data;	//
										
	ros::Subscriber sub_dji_gps;		// Саберы
	ros::Subscriber sub_set_target;		//
	ros::Subscriber sub_precision_gps;	//
	ros::Subscriber sub_action_process;	//
	ros::Subscriber sub_aruco_data;     //
	
	GeoMath::v3 correction_RTK; // смещение GPS DJI относительно RTK
	
	//TODO сделать перечеслением
	uint8_t ctrl_flag;   // режим управления DJI Controller (5 или 8) 
	
	CtrlParameters ctrl; // текущие параметры управления
	
	jukovsky_ros::juk_position_data_msg position_data;  // сообщение, содержит информацию о текущем положении
	jukovsky_ros::juk_control_dji_msg dji_control_msg;  // сообщение, содержит параметры управления для DJI Controller
	
	GeoMath::v3geo precision_position;  // текущее положение по RTK
	GeoMath::v3geo a3_position;  // текущее положение по GPS DJI

	int  precision_pos_quality;  // качество определения положения по RTK
	
	void print_telemetry(const ros::TimerEvent& event); // выводит телеметрию для отладки
	std::map<const char*, std::stringstream> additional_telem_out; 
	ros::Timer timer_telemetry;
	
	static std::map<MODES, const char*> MODES_map;
	static std::map<SUB_MODES, const char*> SUB_MODES_map;
	static std::map<GPS_STATES, const char*> GPS_STATE_map;
	static std::map<int, const char*> FLIGHT_STATUS_map;
	
	// рассчитывает режим и требуемую скрость / смещение в зависимости от текущего положения и положения цели
	static jukovsky_ros::juk_control_dji_msg calculate_control_msg(GeoMath::v3 offset,
		GeoMath::v3 current_velocity,
		double course_need,
		double course_current,
		double speed,
		uint8_t break_mode); 
	
	static bool upd_control_IDLE(NavigationNode* n);
	static bool upd_control_FLY_SIMPLE(NavigationNode* n);
	static bool upd_control_FLY_SAFE(NavigationNode* n);
	static bool upd_control_LANDING_SIMPLE(NavigationNode* n);
	static bool upd_control_LANDING_ARUCO(NavigationNode* n);
	
	static bool upd_control(MODES MODE, NavigationNode* n); 
	
};

#endif // !__NAVIGATION_NODE__
