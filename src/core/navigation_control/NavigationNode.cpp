#include "NavigationNode.h"

std::map<NavigationNode::MODES, const char*> NavigationNode::MODES_map = {
	{ IDLE, "IDLE" },
	{ FLY_SIMPLE, "FLY_SIMPLE" },
	{ FLY_SAFE, "FLY_SAFE" },
	{ LANDING_SIMPLE, "LANDING_SIMPLE" },
	{ LANDING_ARUCO, "LANDING_ARUCO" }
};

std::map<NavigationNode::SUB_MODES, const char*> NavigationNode::SUB_MODES_map = {
	{ BASIC_MODE, "BASIC_MODE" },
	{ FLY_SAFE_UP, "FLY_SAFE_UP" },
	{ FLY_SAFE_CENTER, "FLY_SAFE_CENTER" },
	{ LANDING_SIMPLE_FLY, "LANDING_SIMPLE_FLY" },
	{ LANDING_SIMPLE_LAND, "LANDING_SIMPLE_LAND" },
	{ LANDING_ARUCO_FLY, "LANDING_ARUCO_FLY" },
	{ LANDING_ARUCO_LAND, "LANDING_ARUCO_LAND" }
};

std::map<NavigationNode::GPS_STATES, const char*> NavigationNode::GPS_STATE_map = {
	{ NO_SIGNAL, "NO_SIGNAL" },
	{ BASIC_GPS, "BASIC_GPS" },
	{ RTK, "RTK" }
};

std::map<int, const char*> NavigationNode::FLIGHT_STATUS_map = { 
	{ -1, "no data" },	
	{ 0, "disarmed" },	
	{ 1, "ready to flight" },	
	{ 2, "in the air" }
};

bool NavigationNode::upd_control_IDLE(NavigationNode* n)
{
	n->ctrl.SUB_MODE = SUB_MODES::BASIC_MODE;
	jukovsky_ros::juk_control_dji_msg msg;
	msg.ctrl_mode = jukovsky_ros::juk_control_dji_msg::ctrl_mode_break;
		
	n->ctrl.dji_msg = msg;
	n->ctrl.stable_now = true;
}

bool NavigationNode::upd_control_FLY_SIMPLE(NavigationNode* n)
{
	n->ctrl.SUB_MODE = SUB_MODES::BASIC_MODE;
	GeoMath::v3 position_offset = n->ctrl.target.point_abs - n->current_point_abs;
			
	n->ctrl.dji_msg = calculate_control_msg(position_offset,
		n->current_velocity, 
		n->ctrl.target.course,
		n->position_data.course,
		n->ctrl.target.cruising_speed,
		jukovsky_ros::juk_set_target_data_msg::mode_allow_break_distance);
		
	n->ctrl.stable_now = (n->position_data.dist_to_target <= n->ctrl.target.accurancy);  //цель достигнута, если расстояние меньше заданного
}

bool NavigationNode::upd_control_FLY_SAFE(NavigationNode* n)
{ 
	GeoMath::v3 position_offset = n->ctrl.target.point_abs - n->current_point_abs;
	
	// если расстояние до цели в горизонтали меньше 2м, переход в режим снижения в точку
	if(position_offset.length_xy() < 2) 
	{
		
		n->ctrl.SUB_MODE = SUB_MODES::FLY_SAFE_CENTER;
	}
	else
		// если расстояние по горизонтали больше 5м, переход в режим полета на высоте
		if(position_offset.length_xy() > 5)
	{
		n->ctrl.SUB_MODE = SUB_MODES::FLY_SAFE_UP;
	}
	
	switch (n->ctrl.SUB_MODE)
	{
	case SUB_MODES::FLY_SAFE_UP :

		if (n->current_point_abs.alt - n->homepoint.alt < n->params.args["safe_alt"]) //высота меньше безопасной
			{
				// плдъем вертикально вверх
				GeoMath::v3 sub_position_offset(0, 0, n->params.args["safe_alt"] + n->homepoint.alt - n->current_point_abs.alt);
				n->ctrl.dji_msg = calculate_control_msg(sub_position_offset,
					n->current_velocity,
					n->ctrl.target.course,
					n->position_data.course,
					n->ctrl.target.cruising_speed,
					jukovsky_ros::juk_set_target_data_msg::mode_not_break_distance);
			}
		else
		{
			//полет на текущей высоте в сторону цели
			GeoMath::v3 sub_position_offset =  n->ctrl.target.point_abs - n->current_point_abs;
			sub_position_offset.z = 0;
			
			n->ctrl.dji_msg = calculate_control_msg(sub_position_offset,
				n->current_velocity,
				n->ctrl.target.course,
				n->position_data.course,
				n->ctrl.target.cruising_speed,
				jukovsky_ros::juk_set_target_data_msg::mode_not_break_distance);
		}

		break ;
		
	case SUB_MODES::FLY_SAFE_CENTER :		
		//если расстояние до цели по горизонтали больше 1.5м, аппарат держит высоту и летит к точке
		if(position_offset.length_xy() > 1.5)
		{
			n->ctrl.dji_msg = calculate_control_msg( GeoMath::v3(position_offset.x, position_offset.y, 0),
				n->current_velocity,
				n->ctrl.target.course,
				n->position_data.course,
				n->ctrl.target.cruising_speed,
				n->ctrl.target.break_mode);
		}
		else
		//иначе летит к точке со снижением
		{
			n->ctrl.dji_msg = calculate_control_msg(position_offset,
				n->current_velocity,
				n->ctrl.target.course,
				n->position_data.course,
				n->ctrl.target.cruising_speed,
				n->ctrl.target.break_mode);
		}
				
		if (position_offset.length_xyz() < n->ctrl.target.accurancy)
		{
			n->ctrl.MODE = MODES::FLY_SIMPLE;
			n->ctrl.SUB_MODE = SUB_MODES::BASIC_MODE;					
		}
		break ;
			
	default :
		n->ctrl.SUB_MODE = SUB_MODES::BASIC_MODE;
		break ;
	}
	;
		
	n->ctrl.stable_now = false;
}

bool NavigationNode::upd_control_LANDING_SIMPLE(NavigationNode* n)
{
	if (!n->ctrl.SUB_MODE)
		n->ctrl.SUB_MODE = SUB_MODES::LANDING_SIMPLE_FLY;
		
	GeoMath::v3 position_offset = n->ctrl.target.point_abs - n->current_point_abs;	
		
	switch (n->ctrl.SUB_MODE)
	{
	case SUB_MODES::LANDING_SIMPLE_FLY :
			
		// когда постигли заданной точки, переходим в режим снижения
		if(position_offset.length_xyz() < n->ctrl.target.accurancy)
		{
			n->ctrl.MODE = MODES::LANDING_SIMPLE;
			n->ctrl.SUB_MODE = SUB_MODES::LANDING_SIMPLE_LAND;					
		}
			
		n->ctrl.dji_msg = calculate_control_msg(position_offset,
			n->current_velocity, 
			n->ctrl.target.course,
			n->position_data.course,
			n->ctrl.target.cruising_speed,
			jukovsky_ros::juk_set_target_data_msg::mode_allow_break_distance);
			
		break ;
				
	case SUB_MODES::LANDING_SIMPLE_LAND :
				
		// если аппарат достиг земли (задизармлен), устанавливаем хоумпоинт
		if(n->flight_status < 1)
			n->ctrl.set_homepoint_flag = true;
			
		n->ctrl.dji_msg = calculate_control_msg(GeoMath::v3(0, 0, -100),
			n->current_velocity, 
			0,
			0,
			0.6,
			jukovsky_ros::juk_set_target_data_msg::mode_not_break_distance);
			
		break ;
				
	default :
		break ;
	}		
}

bool NavigationNode::upd_control_LANDING_ARUCO(NavigationNode* n)
{
	// максимальное время после обновления позации маркера
	const long int aruco_timeout = 1000000000;
	
	// если подрежим не установлен или время после обновления позиции маркера больше максимального
	// выполняется полет в целевую точку
	if(!n->ctrl.SUB_MODE || (ros::Time::now() - n->aruco_land.uptime).toNSec() >= aruco_timeout)
	{
		n->ctrl.SUB_MODE = SUB_MODES::LANDING_ARUCO_FLY;
	}
	else
	{
		n->ctrl.SUB_MODE = SUB_MODES::LANDING_ARUCO_LAND;
	}
				
	switch (n->ctrl.SUB_MODE)
	{
	case SUB_MODES::LANDING_ARUCO_FLY :
			
		{
			GeoMath::v3 position_offset = n->ctrl.target.point_abs - n->current_point_abs;
			n->ctrl.dji_msg = calculate_control_msg(position_offset, n->current_velocity, n->ctrl.target.course, n->position_data.course, n->ctrl.target.cruising_speed, jukovsky_ros::juk_set_target_data_msg::mode_allow_break_distance);
		}
		break;

	case SUB_MODES::LANDING_ARUCO_LAND :
		{		
			// целевой точкой становится точка на 2 метра выше той, из котороый аппарат в последний раз видел маркер
			n->ctrl.target.point_abs = n->aruco_land.abs + GeoMath::v3(0, 0, 2);
			
			// расстояние по горизонтали, на котором возможно снижение зависит от высоты
			double max_dist = abs(n->aruco_land.offset.z) / 7 + 0.1;
			double current_dist = n->aruco_land.offset.length_xy();
			
			// направление аппарата должно совпадать с направлением маркера
			n->ctrl.target.course = n->aruco_land.course;
			GeoMath::v2 cC(cos(n->ctrl.target.course*GeoMath::CONST.DEG2RAD), sin(n->ctrl.target.course*GeoMath::CONST.DEG2RAD));
			GeoMath::v2 cN(cos(n->position_data.course*GeoMath::CONST.DEG2RAD), sin(n->position_data.course*GeoMath::CONST.DEG2RAD));

			// пока расстояние до маркера по горизонтали больше максимального, снижения не происходит
			// аппарат центрируется над маркером
			if(current_dist > max_dist)
			{
				n->ctrl.dji_msg.yaw_rate = 0;
				n->ctrl.dji_msg.data_x = n->aruco_land.offset.x;
				n->ctrl.dji_msg.data_y = -n->aruco_land.offset.y;
				n->ctrl.dji_msg.data_z = 0;
				n->ctrl.dji_msg.ctrl_mode = jukovsky_ros::juk_control_dji_msg::ctrl_mode_position_body;
			}
			else
			// расстояние находится в допустимых пределах
			{
				n->ctrl.dji_msg.data_x = n->aruco_land.offset.x;
				n->ctrl.dji_msg.data_y = -n->aruco_land.offset.y;
				n->ctrl.dji_msg.yaw_rate = cC.angle_xy(cN)*GeoMath::CONST.RAD2DEG / 2;
				
				// если погрешность по курсу меньше 2 градусов, происходит снижение
				// иначе аппарат стабилизируется по курсу относительно маркера
				if(abs(n->ctrl.dji_msg.yaw_rate) < 2)
				{
					// коэффициент скорости снижения зависит от расстояния до центра по следующему закону:
					double dist_coeff = abs((max_dist - current_dist) / max_dist);
					
					if (dist_coeff < 0)
						dist_coeff = 0;
					
					//скорость снижения так же зависит от высоты над маркером
					if(n->aruco_land.offset.z > 1.5)
					{
						n->ctrl.dji_msg.data_z = -0.9*dist_coeff;
					}
					else
					{
						n->ctrl.dji_msg.data_z =  -0.2;
						n->ctrl.dji_msg.data_x = n->ctrl.dji_msg.data_x;
						n->ctrl.dji_msg.data_y = n->ctrl.dji_msg.data_y;
					}
				}
				else
				{
					n->ctrl.dji_msg.data_x = 0;
					n->ctrl.dji_msg.data_y = 0;
					n->ctrl.dji_msg.data_z = 0;
				}
				
				n->ctrl.dji_msg.ctrl_mode = jukovsky_ros::juk_control_dji_msg::ctrl_mode_position_body;
			}
		}
		break ;
	default :
			
		break ;
	}
		
	// когда аппарат коснулся земли, посадка продолжается, чтобы он задизармился
	if(n->flight_status < 2)
	{
		n->ctrl.dji_msg.data_x = 0;
		n->ctrl.dji_msg.data_y = 0;
		n->ctrl.dji_msg.data_z = -0.3;
	}
		
	// после дизарма происходит установка нового хоумпоинта
	if(n->flight_status < 1)
		n->ctrl.set_homepoint_flag = true;
		
	n->ctrl.stable_now = false;
}

bool NavigationNode::upd_control(MODES MODE, NavigationNode* n)
{	
	switch (MODE)
	{
	case MODES::IDLE:
		upd_control_IDLE(n);
		break;
		
	case MODES::FLY_SIMPLE:
		upd_control_FLY_SIMPLE(n);
		break;
		
	case MODES::FLY_SAFE:
		upd_control_FLY_SAFE(n);
		break;
		
	case MODES::LANDING_SIMPLE:
		upd_control_LANDING_SIMPLE(n);
		break;
		
	case MODES::LANDING_ARUCO:
		upd_control_LANDING_ARUCO(n);
		break;
		
	default:
		return false;
		break;
	}
	return true;
}

NavigationNode::NavigationNode(int argc, char** argv) 
{
	std::cout.flags(std::ios::fixed);
	std::cout.precision(2);
	
	params.args["safe_alt"] = 20;
	params.args["enable_emlid"] = 1;
	params.args["gear_height"] = 2;
	params.args["navigation_telem"] = 1;
	
	usleep(5000000);
	
	correction_RTK = GeoMath::v3(0.01, 0.01, 0.0);
	
	params.parse(argc, argv);
	std::cout << c(32, "@Parameters JUK_NAVIGATION_NODE: ") << std::endl;
	for (auto& arg : params.args)
	{
		std::string key;
		if (ros::param::search(arg.first, key))
		{
			ros::param::get(key, arg.second);
		}
		std::cout << c(32, "~~") << arg.first << ": " << c(32, arg.second) << std::endl;
	}
	
	node_start_time = ros::Time::now();
	aruco_land.uptime = node_start_time;
	home_uptime = node_start_time;
	last_telemetry = node_start_time;
	
	pub_dji_control = nh.advertise<jukovsky_ros::juk_control_dji_msg>("JUK/CONTROL_DJI", 1);
	pub_position_data = nh.advertise<jukovsky_ros::juk_position_data_msg>("JUK/POSITION_DATA", 1);
	
	ctrl.target.cruising_speed = 1;
	ctrl.target.accurancy = 0.3;
	ctrl.target.course = 0;
	yaw_rate = 0;
	precision_pos_quality = 6;
	ctrl.target.break_mode = jukovsky_ros::juk_set_target_data_msg::mode_allow_break_distance;

	sub_aruco_data = nh.subscribe("JUK/ARUCO/DATA", 2, &NavigationNode::aruco_callback, this);
	sub_dji_gps = nh.subscribe("JUK/DJI/GPS", 1, &NavigationNode::gps_callback, this);
	sub_set_target = nh.subscribe("JUK/TARGET", 1, &NavigationNode::set_target_callback, this);
	sub_action_process = nh.subscribe("JUK/NAVIGATION_ACTIONS", 10, &NavigationNode::action_process_callback, this);
	
	if (params.args["enable_emlid"] == 1)
	{
		sub_precision_gps = nh.subscribe("REACH_EMLID_DATA", 1, &NavigationNode::precision_gps_callback, this);
	}
	
	stable_now = false;
	stable_last = false;
	stable_time = -1;
	
	ctrl.set_homepoint_flag = true;
	
	this->timer_telemetry = nh.createTimer(ros::Duration(0.2), &NavigationNode::print_telemetry, this);
	
	for (int i = 0; i < 30; i++)
	{
		std::cout << std::endl;
	}
}

jukovsky_ros::juk_control_dji_msg 
NavigationNode::calculate_control_msg(GeoMath::v3 offset, GeoMath::v3 current_velocity, double course_need, double course_current, double abs_speed, uint8_t break_mode)
{
	jukovsky_ros::juk_control_dji_msg ans;
	ans.ctrl_mode = jukovsky_ros::juk_control_dji_msg::ctrl_mode_velocity;
	
	const double max_break_acc = 5;
	const double max_force_acc = 0.7;
	double need_abs_speed;
	
	double max_z_speed = 5;
	
	
	need_abs_speed = std::min(abs_speed, current_velocity.length_xyz() + max_force_acc);
	double addition_break_time = 0.1;
	double current_distance = offset.length_xyz();
	
	double break_distance = 0;
	
	switch (break_mode)
	{
	case jukovsky_ros::juk_set_target_data_msg::mode_allow_break_distance :
		
		break_distance = ((abs_speed*abs_speed) / (2*max_break_acc));
		
		if (current_distance < break_distance + addition_break_time*need_abs_speed + 1.5)
		{
			ans.ctrl_mode = jukovsky_ros::juk_control_dji_msg::ctrl_mode_position_ground;
		}
		break ;
		
	case jukovsky_ros::juk_set_target_data_msg::mode_not_break_distance :
		break;
	}
	
	GeoMath::v2 cC(cos(course_current*GeoMath::CONST.DEG2RAD), sin(course_current*GeoMath::CONST.DEG2RAD));
	GeoMath::v2 cN(cos(course_need*GeoMath::CONST.DEG2RAD), sin(course_need*GeoMath::CONST.DEG2RAD));

	ans.yaw_rate = -cC.angle_xy(cN)*GeoMath::CONST.RAD2DEG;

	if (fabs(ans.yaw_rate) > 5)
	{
		ans.yaw_rate = ans.yaw_rate / 5;
	}
	
	GeoMath::v3 velocity_need = offset.normalize_xyz(need_abs_speed);

	if (abs(ans.yaw_rate) < 2.5)
	{
		if (velocity_need.z > max_z_speed)
			velocity_need = velocity_need * (max_z_speed / velocity_need.z);
	
		if (ans.ctrl_mode == jukovsky_ros::juk_control_dji_msg::ctrl_mode_position_ground)
		{
			velocity_need.x = offset.x;
			velocity_need.y = offset.y;
			velocity_need.z = offset.z / 2; 
		}
	}
	else
	{
		ans.ctrl_mode = jukovsky_ros::juk_control_dji_msg::ctrl_mode_velocity;
		velocity_need.x = 0;
		velocity_need.y = 0;
		velocity_need.z = 0;
	}
	
	ans.data_x = velocity_need.x; 
	ans.data_y = velocity_need.y;
	ans.data_z = velocity_need.z;
	
	return ans ;
}

void NavigationNode::aruco_callback(const jukovsky_ros::juk_aruco_module_data::ConstPtr& input)
{
	aruco_land.offset = GeoMath::v3(-input->x / 100, -input->y / 100, -input->z / 100);
	aruco_land.course = position_data.course +  input->course * GeoMath::CONST.RAD2DEG;
	aruco_land.uptime = ros::Time::now();
	
	aruco_land.abs = current_point_abs + GeoMath::v3(aruco_land.offset.x, -aruco_land.offset.y, 1.5 + aruco_land.offset.z).rotateXY(position_data.course*GeoMath::CONST.DEG2RAD);
}

void
NavigationNode::action_process_callback(const jukovsky_ros::juk_navigation_actions_msg::ConstPtr& input)
{
	int action = input->action;
	
	switch (action)
	{
		
	case jukovsky_ros::juk_navigation_actions_msg::set_homepoint :
		ctrl.set_homepoint_flag = true;
		break ;
	
	case jukovsky_ros::juk_navigation_actions_msg::pause :
		
		pause_target.accurancy = 0;
		pause_target.point_abs = current_point_abs;
		pause_target.cruising_speed = 1;
		pause_target.break_mode = 0;
		
		paused = true;
		break;

	case jukovsky_ros::juk_navigation_actions_msg::unpause :
		paused = false;
		break;
		
	default :
		break ;
	}
}

void
NavigationNode::gps_callback(const jukovsky_ros::juk_dji_gps_msg::ConstPtr& input)
{
	auto now = ros::Time::now();
	
	a3_position = GeoMath::v3geo(input->lat*GeoMath::CONST.RAD2DEG, input->lng*GeoMath::CONST.RAD2DEG, input->alt); 
	
	GPS_STATE = GPS_STATES::BASIC_GPS;
	
	bool emlid_ok = (params.args["enable_emlid"]&&(precision_pos_quality == 1 || precision_pos_quality == 2)) || !params.args["enable_emlid"];
	
	if (emlid_ok && (now - precision_pos_uptime).toNSec() < 1e9)
	{
		correction_RTK = precision_position - a3_position;
		correction_RTK.z = 0;
		
		GPS_STATE = GPS_STATES::RTK;
	}
	
	current_point_abs = a3_position + correction_RTK;
	
	current_velocity = GeoMath::v3(input->vx, input->vy, input->vz);
	
	if (((now - node_start_time).toNSec() > 5e9)  && input->quality)
	{
	
		flight_status = input->flight_status;
	
		if (ctrl.set_homepoint_flag)
		{
			ctrl.target.course = input->course*GeoMath::CONST.RAD2DEG;
		
			homepoint_course = input->course;
			homepoint = current_point_abs;
		
			ctrl.target.point_abs = homepoint;
		
			ctrl.set_homepoint_flag = false;
		
			ctrl.MODE = MODES::IDLE;
			ctrl.SUB_MODE = SUB_MODES::BASIC_MODE;
		
			home_uptime = ros::Time::now();
		}
	
		current_point_home = current_point_abs - homepoint;
		
		position_data.alt = current_point_abs.alt;
		position_data.lat = current_point_abs.lat;
		position_data.lng = current_point_abs.lng;
	
		position_data.x = current_point_home.x;
		position_data.y = current_point_home.y;
		position_data.z = current_point_home.z;

		position_data.course = input->course*GeoMath::CONST.RAD2DEG;
	
		//GeoMath::v3 position_offset = current_target.point_abs - current_point_abs;
		GeoMath::v3 position_offset = ctrl.target.point_abs - current_point_abs;
	
		if (!ctrl.set_homepoint_flag)
		{		
			if (!upd_control(ctrl.MODE, this))
			{
				ctrl.dji_msg.data_x = 0;
				ctrl.dji_msg.data_y = 0;
				ctrl.dji_msg.data_z = 0;
				ctrl.dji_msg.ctrl_mode = jukovsky_ros::juk_control_dji_msg::ctrl_mode_velocity;
				ctrl.dji_msg.yaw_rate = 0;
			
				ctrl.stable_now = false;
			}
		
			if (paused)
			{
				ctrl.dji_msg = calculate_control_msg(pause_target.point_abs-current_point_abs,
					current_velocity,
					0,
					0,
					1,
					0);
				ctrl.stable_now = false;
				
			}
			
			dji_control_msg = ctrl.dji_msg;
			stable_now = ctrl.stable_now;
		}
		else
		{
			stable_now = true;
			dji_control_msg.data_x = 0;
			dji_control_msg.data_y = 0;
			dji_control_msg.data_z = 0;
		}
	
		position_data.dist_to_target = (current_point_abs - ctrl.target.point_abs).length_xyz();
	
		GeoMath::v3 pos_from_home = (GeoMath::v3geo(position_data.lat, position_data.lng, position_data.alt) - homepoint).rotateXY(homepoint_course);
	
		position_data.x = pos_from_home.x;
		position_data.y = pos_from_home.y;
		position_data.z = pos_from_home.z;
	
		if (stable_now)
		{
			if (!stable_last)
			{
				stable_start = ros::Time::now();
			}
			stable_time = (ros::Time::now() - stable_start).sec;
			position_data.stable_time = stable_time;
		}
		else
		{
			stable_time = 0;
			position_data.stable_time = 0;
		}	
		
		position_data.paused = paused;
		
		stable_last = stable_now;
		
		pub_dji_control.publish(dji_control_msg);
		pub_position_data.publish(position_data);
	}
	
}


void 
NavigationNode::precision_gps_callback(const jukovsky_ros::reach_msg::ConstPtr& in)
{
	precision_pos_uptime = ros::Time::now();
	precision_pos_quality = in->quality;
	
	precision_position.lat = in->lat;
	precision_position.lng = in->lng;
	precision_position.alt = current_point_abs.alt;
}

void NavigationNode::set_target_callback(const jukovsky_ros::juk_set_target_data_msg::ConstPtr& target)
{
	ctrl.MODE = (MODES)target->fly_mode;
	ctrl.SUB_MODE = SUB_MODES::BASIC_MODE;
	this->ctrl.target.break_mode = target->break_distance_mode;
		
	this->ctrl.target.cruising_speed = target->speed;
	this->ctrl.target.accurancy = target->acc;
		
	switch (target->system)
	{
	case jukovsky_ros::juk_set_target_data_msg::system_absolut :
		this->ctrl.target.point_abs = GeoMath::v3geo(target->data_x, target->data_y, target->data_z + homepoint.alt);
		break ;
		
	case jukovsky_ros::juk_set_target_data_msg::system_home :
		this->ctrl.target.point_abs = homepoint + GeoMath::v3(target->data_x, target->data_y, target->data_z);
		break ;
		
	case jukovsky_ros::juk_set_target_data_msg::system_offset_from_target :
		this->ctrl.target.point_abs = this->ctrl.target.point_abs + GeoMath::v3(target->data_x, target->data_y, target->data_z).rotateXY(this->position_data.course*GeoMath::CONST.DEG2RAD);
		break ;	
		
	case jukovsky_ros::juk_set_target_data_msg::system_offset_from_here :
		this->ctrl.target.point_abs = GeoMath::v3geo(this->position_data.lat, this->position_data.lng, this->position_data.alt) + GeoMath::v3(target->data_x, target->data_y, target->data_z).rotateXY(this->position_data.course*GeoMath::CONST.DEG2RAD);
		break ;	
	}
	
	switch (target->course_mode)
	{
	case jukovsky_ros::juk_set_target_data_msg::course_abs:		
		{
			this->ctrl.target.course = target->course;
		}
		break;
		
	case jukovsky_ros::juk_set_target_data_msg::course_add:		
		{
			double rotate_rad = target->course*GeoMath::CONST.DEG2RAD;
			
			this->position_data.course = GeoMath::CONST.RAD2DEG*(GeoMath::v2(cos(this->position_data.course*GeoMath::CONST.DEG2RAD), sin(this->position_data.course*GeoMath::CONST.DEG2RAD)).rotateXY(rotate_rad)).angle_xy(GeoMath::v2(1, 0));
		}
		break;
		
	case jukovsky_ros::juk_set_target_data_msg::course_from_move_dir:		
		{
			double course_rad = target->course*GeoMath::CONST.DEG2RAD;
			
			double course_deg = (this->ctrl.target.point_abs - GeoMath::v3geo(this->position_data.lat, this->position_data.lng, this->position_data.alt)).rotateXY(course_rad).angle_xy(GeoMath::v3(1, 0, 0))*GeoMath::CONST.RAD2DEG;
			
			if (course_deg != course_deg)
			{
				course_deg = this->position_data.course;
			}
			
			this->ctrl.target.course = course_deg;
		}
		break;
		
	default:
		break;
	}
}
void NavigationNode::print_telemetry(const ros::TimerEvent& event)
{
	auto now = ros::Time::now();
	if ((now - last_telemetry).nsec >  1000000000.0 * 0.15 && params.args["navigation_telem"] == 1)
	{
		
		
		std::stringstream out;
		out.flags(std::ios_base::fixed);
		out.precision(2);
				
		out << grn("STATE:\n\t") << MODES_map[ctrl.MODE] << std::endl;
		out << grn("SUB STATE:\n\t") << SUB_MODES_map[ctrl.SUB_MODE] << std::endl;
		out << grn("GPS STATE:\n\t") << GPS_STATE_map[GPS_STATE] << std::endl;
		out << grn("FLIGHT STATUS:\n\t") << FLIGHT_STATUS_map[(int)flight_status] << std::endl;
		out << grn("POSITION:\n\t") << GeoMath::v3(position_data.x, position_data.y, position_data.z) << " c: " << position_data.course << std::endl;
		out << grn("RTK CORRECTION:\n\t") << correction_RTK << std::endl;
		out << grn("TARGET:\n\t") << (this->ctrl.target.point_abs - homepoint) << " c: " << this->ctrl.target.course << std::endl;
		out.precision(8);
		out << "\t" << "lat: " << this->ctrl.target.point_abs.lat << " lat: " << this->ctrl.target.point_abs.lng << " alt: " << this->ctrl.target.point_abs.alt - homepoint.alt << std::endl;
		out.precision(2);
		out << grn("STABLE TIME:\n\t") << stable_time << std::endl;
		
		if ((now - aruco_land.uptime).sec < 3)
		{
			out << grn("ARUCO POSITION:\n\t") << aruco_land.offset << std::endl;
		}
		else
		{
			out << grn("ARUCO POSITION:\n\t") << "x: ~ \t y: ~ \t z: ~" << std::endl;
		}
		
		
		out << grn("_____________________________") << std::endl;
		
		if ((now - home_uptime).toNSec() < 6e9)
		{
			out << std::endl;
			out << green_b("~~~~~~~~~~~~~~~~\nSET HOMEPOINT  ") << green_b(int((now - home_uptime).toSec())) << green_b("\n~~~~~~~~~~~~~~~~") << std::endl;
		}
		
		if (paused)
		{
			out << std::endl;
			out << red_b("~~~~~~\nPAUSED\n~~~~~~") << std::endl;
		}
		
		for (auto& t : additional_telem_out)
		{
			out << t.first << ":\n\t" << t.second.str() << std::endl;
		}
		
		last_telemetry = now;
		
		std::string telem_txt = out.str();
		
		int telem_heigth = 0;
		
		for (const auto& c : telem_txt)
		{
			if (c == '\n')
				telem_heigth++;
		}
		
		CLEAR(telem_heigth + 1);
		std::cout << out.str() << std::endl;
	}
}