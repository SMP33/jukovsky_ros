#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/video/tracking.hpp>

#include <GeoMath.h>
#include <jukovsky_ros/juk_aruco_module_action.h>
#include <jukovsky_ros/juk_aruco_module_data.h>
 
using namespace cv;
using namespace std;

#define drawCross( img, center, color, d )                                 \
line( img, Point( 300+center.x - d, 300+center.y - d ), Point( 300+center.x + d, 300+center.y + d ), color, 2, CV_AA, 0); \
line( img, Point( 300+center.x + d, 300+center.y - d ), Point( 300+center.x - d, 300+center.y + d ), color, 2, CV_AA, 0 )

#ifndef CURSOR_H
#define	CURSOR_H

#define CLEAR(rows)               printf("\033[%02dA\033[0J",rows+1)
//  
#define HOME                printf("\033[1;1H")
#define STORE_CURSOR        printf("\033[s")
#define RESET_CURSOR        printf("\033[u")
#define CLEAR_EOL           printf("\033[K")
#define CURSOR(row, column) printf("\033[%02d;%02dH", row, column)



//  
#define RESET_COLOR printf("\033[0m")

#define BLACK       0
#define RED         1
#define GREEN       2
#define YELLOW      3
#define BLUE        4
#define MAGENTA     5
#define CYAN        6
#define WHITE       7
#define UNDEF       -1

//  
#define NONE        0
#define BOLD        1
#define DIM         2
#define UNDERLINE   4
#define BLINK       5
#define REVERSE     7

#define COLOR(bgcolor, fgcolor)         printf("\033[%02d;%02dm", (bgcolor + 30), (fgcolor + 40))
#define COLOR_A(bgcolor, fgcolor, attr) printf("\033[%02d;%02d;%1dm", (bgcolor + 30), (fgcolor + 40), attr)
#endif	/* CURSOR_H */

#define ROTATE_

//Переносит угол в диапазон 0 - 2*Pi
void angle_normalize(double& rad)
{
	while (rad < 0)
	{
		rad += 2*GeoMath::CONST.Pi;
	}
	
	while (rad >= 2*GeoMath::CONST.Pi)
	{
		rad -= 2*GeoMath::CONST.Pi;
	}
}

//Описывает размер и положение маркера, является элементом карты маркеров
struct single_marker
{
	single_marker()
	{
	}
	
	single_marker(int size_, GeoMath::v3 abs_pos_) :
		size(size_),
		abs_pos(abs_pos_)
	{
	}
	
	single_marker(int size_, GeoMath::v3 abs_pos_,int max_quality_)
		: size(size_)
		, abs_pos(abs_pos_)
		, max_quality(max_quality_)
	{
	}
	
	int size=0; //длина стороны см
	GeoMath::v3 pos_last; //позиция относительно камеры на прерыдущей итерации 
	GeoMath::v3 pos_now; //текущая позиция относительно камеры
	GeoMath::v3 abs_pos; //координаты маркера на карте
	
	double course=0; //угол по курсу, радианы 
	int qulity=0; //насколько хорошо виден маркер
	int max_quality=5;
};

//Преобразует матрицу поворота в углй Эйлера
Vec3d rotationMatrixToEulerAngles(Mat &R)
{
     
	float sy = sqrt(R.at<double>(0, 0) * R.at<double>(0, 0) +  R.at<double>(1, 0) * R.at<double>(1, 0));
 
	bool singular = sy < 1e-6;    // If
 
	float x, y, z;
	if (!singular)
	{
		x = atan2(R.at<double>(2, 1), R.at<double>(2, 2));
		y = atan2(-R.at<double>(2, 0), sy);
		z = atan2(R.at<double>(1, 0), R.at<double>(0, 0));
	}
	else
	{
		x = atan2(-R.at<double>(1, 2), R.at<double>(1, 1));
		y = atan2(-R.at<double>(2, 0), sy);
		z = 0;
	}
	return Vec3d(x, y, z);    
}
//Преобразует OpenCVшный rvec в углы Эйлера
Vec3d rvec2Euler(Vec3d rvec)
{
	Mat rot_mat;
	cv::Rodrigues(rvec, rot_mat);
				
	return rotationMatrixToEulerAngles(rot_mat);
}

//Класс осуществляет прием, обработку изображения и публикует результат в топики
class ImageConverter
{
	ros::NodeHandle nh_;
	image_transport::ImageTransport it_;
	image_transport::Subscriber image_sub_;
	ros::Subscriber action_sub;
	image_transport::Publisher image_pub_img;
	ros::Publisher data_pub;
	image_transport::Publisher image_pub_gray;
	image_transport::Publisher image_pub_canny;
	
	GeoMath::v3 last_abs_mrk_pos;
	
	ros::Time last_cb; //премя последнего получения изображения
	
	//параметры камеры
	cv::Mat camera_matrix_;
	cv::Mat dist_coeffs_;

	//карта маркеров
	map<int, single_marker> markers;

	ros::Time last_img_pub;
	
public:
	ImageConverter()
		: it_(nh_),
	{
		last_cb = ros::Time::now();
		last_img_pub = last_cb;
		mrk_id = 172;
		mrk_size = 540;
		
		
		action_sub = nh_.subscribe("JUK/ARUCO/ACTION",
			1,
			&ImageConverter::actionCb,
			this);

		image_pub_canny = it_.advertise("JUK/ARUCO/CANNY", 1);
		data_pub = nh_.advertise<jukovsky_ros::juk_aruco_module_data>("JUK/ARUCO/DATA", 1);
		image_pub_img = it_.advertise("JUK/ARUCO/IMG", 1);
		
		camera_matrix_ = (cv::Mat1f(3, 3) << 
305.3469452976, 0.0000000000, 309.8420253988,
  0.0000000000, 305.8877199607, 244.1667919667,
  0.0000000000, 0.0000000000, 1.0000000000 );
	
		dist_coeffs_ = (cv::Mat1f(5, 1) << -0.2606891432, 0.0561859421, -0.0003313691, -0.0009746269, -0.0047886394);
		
		image_sub_ = it_.subscribe("/main_camera/image_raw/throttled",
			1,
			&ImageConverter::imageCb,
			this);
		cout << "ARUCO DETECTION START" << endl;
		cout << endl;
		
		//Задаем положение и размер каждого маркера на карте

		markers[269] = single_marker(900, GeoMath::v3(0, 0, 0),2);
		
		markers[0] = single_marker(100, GeoMath::v3(0, 0, 0));
		
		markers[1] = single_marker(100, GeoMath::v3(-30, -30, 0));
		markers[2] = single_marker(100, GeoMath::v3(30, -30, 0));
		markers[3] = single_marker(100, GeoMath::v3(30, 30, 0));
		markers[4] = single_marker(100, GeoMath::v3(-30, 30, 0));
		
		markers[11] = single_marker(50, GeoMath::v3(-15, -15, 0)); 
		markers[12] = single_marker(50, GeoMath::v3(15, -15, 0));
		markers[13] = single_marker(50, GeoMath::v3(15, 15, 0));
		markers[14] = single_marker(50, GeoMath::v3(-15, 15, 0));
		
		markers[101] = single_marker(60, GeoMath::v3(0, -22, 0));
		markers[102] = single_marker(60, GeoMath::v3(22, 0, 0));
		markers[103] = single_marker(60, GeoMath::v3(0, 22, 0));
		markers[104] = single_marker(60, GeoMath::v3(-22, 0, 0));
	
	}

	~ImageConverter()
	{
	}
	
	//не используется
	void actionCb(const jukovsky_ros::juk_aruco_module_action::ConstPtr& msg)
	{

	}

	//вызывается при получении нового изображения
	void imageCb(const sensor_msgs::ImageConstPtr& msg)
	{		
		
		auto t1 = ros::Time::now();
		cv_bridge::CvImagePtr cv_ptr;
		cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);		
		Mat img = cv_ptr->image.clone();

		//id положения камеры - не используется
		int rotation_flag = 1;
		
		vector<int> markerIds;
		vector<vector<Point2f>> markerCorners, rejectedCandidates;
		Ptr<aruco::Dictionary> markerDictionary = aruco::getPredefinedDictionary(aruco::PREDEFINED_DICTIONARY_NAME::DICT_4X4_1000);
		cv::Ptr<cv::aruco::DetectorParameters> parameters = aruco::DetectorParameters::create();
		parameters->adaptiveThreshWinSizeMin=10;
		std_msgs::Header header; 
		aruco::detectMarkers(img, markerDictionary, markerCorners, markerIds,parameters);

		vector<GeoMath::v3> position_vec(0); //содержит положение каждого найденного маркера относительно кармеры
		vector<GeoMath::v3> abs_pos_vec(0); //содержит положение каждого найденного маркера с СК, связанной с картой
		vector<double> course_vec(0); //содержит курс каждого найденного маркера
		
		//обрабатываем каждый найденный маркер
		for (int i = 0; i < markerCorners.size(); i++)
		{			
			//если карта не содержит найденный маркер, пропускаем его
			if (markers.find(markerIds[i]) == markers.end())
			{
				vector<vector<Point2f>> corners_ = { markerCorners[i]};
				vector<int> ids_ = { markerIds[i]};
				aruco::drawDetectedMarkers(img, corners_, ids_);
				continue;
			}
			
			//определяем его позицию относительно камеры
			single_marker& mp = markers[markerIds[i]];
			vector<Point2f> corn(markerCorners[i]);
			vector<cv::Vec3d> rvecs, tvecs, euler;
			vector<vector<Point2f>> corners(0);
			corners.push_back(corn);
			cv::aruco::estimatePoseSingleMarkers(corners,
				mp.size/10,
				camera_matrix_,
				dist_coeffs_,
				rvecs,
				tvecs);
			cv::aruco::drawAxis(img, camera_matrix_, dist_coeffs_, rvecs, tvecs, mp.size / 20);

			//позиция относительно камеры с учетом поворотов маркера относительно камеры по трем осям
			double course = rvec2Euler(rvecs[0])[2];
			angle_normalize(course);
			Mat rvec(rvecs[0]), tvec(tvecs[0]);
			Mat R;
			cv::Rodrigues(rvec, R);
			R = R.t();
			Mat T = -R * tvec;
			
			Vec3d v3d(T);
			GeoMath::v3 offset(v3d[0], v3d[1], v3d[2]);
			offset.y = -offset.y;
			offset = offset.rotateXY(course);

			//обновляем позацию маркера
			mp.pos_now = offset;
			mp.course = course;

			//цвет отрисовки контура маркера
			Scalar color = Scalar(0, 0, 255);
			
			//если маркер сместился в приемлимых диапазонах, повышаем показатель качества определения
			if ((mp.pos_now - mp.pos_last).length_xyz() < 400 )
			{
				mp.qulity++;
				
				//если качество удовлетворительное, используем маркер дальше
				if (mp.qulity >= mp.max_quality)
				{
					color = Scalar(0, 255, 0);
					mp.qulity = mp.max_quality;
					
					//при большом отдалении от камеры, не учитываем в позиции углы поворота маркера
					if (arcLength(markerCorners[i], true) < 600)
						{
							color = Scalar(0, 255, 255);
							Vec3d pos_simple(tvec);
							mp.pos_now = GeoMath::v3(-pos_simple[0], -pos_simple[1], pos_simple[2]);
						}
					
					position_vec.push_back(mp.pos_now + mp.abs_pos);
					abs_pos_vec.push_back(mp.abs_pos);
					course_vec.push_back(mp.course);
					
				}	
			}
			//если маркер за 1 итерацию сместился слишком сильно, не обнуляем его показатель качества определения
			else
			{
				mp.qulity = 0;
			}
			
			mp.pos_last = mp.pos_now;
			
			//отрисовка
			vector<vector<Point>> contours(1);
			for (auto c : corners[0])
			{
				contours[0].push_back(c);
			}
			drawContours(img, contours, 0, color, 2);
		}
		
		int s_c = course_vec.size();
		int s_p = position_vec.size();
				
		GeoMath::v2 direction(0, 0);
		double course = 0;
		GeoMath::v3 pos;
		
		if(s_c>0)
		{
			//усредняем свое положение и курс
			for (int i = 0; i < s_c; i++)
			{
				direction = direction + GeoMath::v2(cos(course_vec[i]), sin(course_vec[i]));
				pos = pos + position_vec[i];
			}

			pos = pos / s_p;
			course = (direction).angle_xy(GeoMath::v2(1,0));
			
			jukovsky_ros::juk_aruco_module_data data_msg;
			
			data_msg.x = -pos.y;
			data_msg.y = -pos.x;
			data_msg.z =  pos.z;
			
			data_msg.course = course;
			data_pub.publish(data_msg);
		}
		
		//если настало время публикуем картинку
		auto now = ros::Time::now();
		int delay = (now - last_cb).nsec;
		last_cb = now;
		if ((now - last_img_pub).nsec > 1e8)
		{
			header.stamp = ros::Time::now();
			
			auto img_center = Point(img.cols, img.rows)/2;
			auto r_v_1 = Point(30, 30);
			auto r_v_2 = Point(30, -30);
			
			line(img, img_center - r_v_1, img_center + r_v_1, Scalar(0, 255, 0), 1);
			line(img, img_center - r_v_2, img_center + r_v_2, Scalar(0, 255, 0), 1);
			
			putText(img, to_string((int)(1e9 / delay)), Point(10, 20), FONT_HERSHEY_COMPLEX_SMALL, 1, Scalar(0, 255, 0), 1, CV_AA);
		
			cv_bridge::CvImage out_img = cv_bridge::CvImage(header, sensor_msgs::image_encodings::TYPE_8UC3, img);
			image_pub_img.publish(out_img.toImageMsg());
		
			last_img_pub = now;
		}
	}
};




int main(int argc, char** argv)
{
	
	ros::init(argc, argv, "JUK_ARUCO");
	ImageConverter ic;
	
	
	ros::spin();
	return 0;
}


